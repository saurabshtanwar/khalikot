# Generated by Django 4.0.4 on 2022-05-02 01:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('blog', '0001_initial'),
        ('master', '0002_alter_city_city_id_alter_faq_faq_id_and_more'),
        ('packages', '0002_tourist_attrations_tourist_attrations_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Scroll_Image',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('image', models.ImageField(null=True, upload_to='images/')),
                ('default', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Side_content',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Side_Image', models.ImageField(upload_to='images/')),
                ('Title', models.CharField(max_length=200, null=True)),
                ('Details', models.TextField(max_length=1000, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tourist_attr_view_home',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attraction', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='packages.tourist_attrations')),
            ],
        ),
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('testimonial', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='master.feedback')),
            ],
        ),
        migrations.CreateModel(
            name='Pakages_view_home',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('package', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='packages.packagedetails')),
            ],
        ),
        migrations.CreateModel(
            name='Blog_view_home',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('blog', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='blog.post')),
            ],
        ),
    ]
