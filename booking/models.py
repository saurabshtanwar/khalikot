from django.db import models
from django.conf import settings
from django.utils import timezone
from packages.models import *
# Create your models here.


class Booking(models.Model):
    booking_id = models.CharField(max_length=6, primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, null=True)
    #source = models.CharField(max_length=20)
    #destination = models.CharField(max_length=20)
    package = models.ForeignKey(
        PackageDetails, on_delete=models.CASCADE, null=True)
    arrival_date = models.DateField(null=True)
    departure_date = models.DateField(null=True)
    no_of_person = models.PositiveIntegerField(default=0)
    amount = models.PositiveIntegerField()
