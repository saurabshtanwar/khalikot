from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Header)
admin.site.register(Bottom)
admin.site.register(Vision)
admin.site.register(Values)
admin.site.register(Mission)
admin.site.register(Header_to_Show)
