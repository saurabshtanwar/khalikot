from django.contrib import admin
from .models import *


admin.site.register(PackageDetails)
admin.site.register(Package_Image)
admin.site.register(Tourist_Attractions)
admin.site.register(Tourist_Attractions_Image)
