from email.mime import image
from django.db import models
from django.conf import settings
from django.utils import timezone
from packages.models import *
from master.models import *
from blog.models import *
from home.models import *
# Create your models here.


class Header(models.Model):
    Header_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Explore_Locations(models.Model):
    Explore_Locations_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Popular_Destinations(models.Model):
    Popular_Destinations_image = models.ImageField(
        upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)


class Bottom(models.Model):
    Bottom_image = models.ImageField(upload_to='images/', null=True)
    Title = models.CharField(max_length=200, null=True)
    Details = models.TextField(max_length=1000, null=True)
