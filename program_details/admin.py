from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Header)
admin.site.register(Explore_Locations)
admin.site.register(Popular_Destinations)
admin.site.register(Bottom)
