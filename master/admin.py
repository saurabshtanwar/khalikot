from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(State)
admin.site.register(City)
admin.site.register(Tax)
admin.site.register(Offer)
admin.site.register(FAQ_category)
admin.site.register(FAQ)
admin.site.register(Feedback)
admin.site.register(Payment_Method)
